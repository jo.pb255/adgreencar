package com.example.kangaroo.adgreencars;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kangaroo.adgreencars.model.DriverModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class DriverAdapter extends RecyclerView.Adapter<DriverAdapter.ViewHolder> {

    Context context;
    ArrayList<DriverModel> driverModels;

    public DriverAdapter(Context context, ArrayList<DriverModel> driverModels) {
        this.context = context;
        this.driverModels = driverModels;
    }


    @NonNull
    @Override
    public DriverAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DriverAdapter.ViewHolder viewHolder, int i) {

        final DriverModel model = driverModels.get(i);

        viewHolder.tvName.setText(model.getName());
        viewHolder.tvPhone.setText(model.getPhone());


        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
                String[] animals = {"Edit", "Delete"};
                builderSingle.setItems(animals, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0: // แก้ไข
                                Intent intent = new Intent(context, EditDriverDataActivity.class);
                                intent.putExtra("Name", model.getName());
                                intent.putExtra("Phone", model.getPhone());
                                intent.putExtra("Address", model.getAddress());
                                intent.putExtra("Email", model.getEmail());
                                intent.putExtra("Password", model.getPassword());
                                intent.putExtra("id", model.getId());
                                context.startActivity(intent);
                                break;
                            case 1: // ลบ

                                FirebaseDatabase.getInstance().getReference("Drivers").child(model.getId())
                                        .removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Toast.makeText(context, "Delete Successful", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, "Delete unsuccessful", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                                break;
                        }
                    }
                });
                builderSingle.show();
                return true;
            }
        });

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, DriverdetailActivity.class);
                intent.putExtra("Name", model.getName());
                intent.putExtra("Phone", model.getPhone());
                intent.putExtra("Address", model.getAddress());
                intent.putExtra("Email", model.getEmail());
                intent.putExtra("Password", model.getPassword());
                intent.putExtra("id", model.getId());

                context.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return driverModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvName, tvPhone;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvPhone = itemView.findViewById(R.id.tvPhone);


        }

    }

}
