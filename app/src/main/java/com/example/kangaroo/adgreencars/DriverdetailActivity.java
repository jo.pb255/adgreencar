package com.example.kangaroo.adgreencars;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class DriverdetailActivity extends AppCompatActivity {

    private TextView tvShowName, tvShowPhone, tvShowAddress, tvShowEmail, tvShowPassword;
    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driverdetail);

        toolbar = findViewById(R.id.toolbar_list_phone_activity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(" Data Driver");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvShowName = findViewById(R.id.tvShowName);
        tvShowPhone = findViewById(R.id.tvShowPhone);
        tvShowAddress = findViewById(R.id.tvShowAddress);
        tvShowEmail = findViewById(R.id.tvShowEmail);
        tvShowPassword = findViewById(R.id.tvShowPassword);



        String Name = getIntent().getStringExtra("Name");
        String Phone = getIntent().getStringExtra("Phone");
        String Address = getIntent().getStringExtra("Address");
        String Email = getIntent().getStringExtra("Email");
        String Password = getIntent().getStringExtra("Password");
        String id = getIntent().getStringExtra("id");


        tvShowName.setText(Name);
        tvShowPhone.setText(Phone);
        tvShowAddress.setText(Address);
        tvShowEmail.setText(Email);
        tvShowPassword.setText(Password);

        Log.d("asdasdas", "onCreate: "+Name);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        startActivity(new Intent(getApplicationContext(), ListDriverActivity.class));
        return true;
    }
}
