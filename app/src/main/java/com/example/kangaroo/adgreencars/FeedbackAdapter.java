package com.example.kangaroo.adgreencars;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kangaroo.adgreencars.model.Feedback;

import java.util.ArrayList;

public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolder> {

    Context context;
    ArrayList<Feedback> feedbacks;

    public FeedbackAdapter(Context context, ArrayList<Feedback> feedbacks) {
        this.context = context;
        this.feedbacks = feedbacks;
    }


    @NonNull
    @Override
    public FeedbackAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.activity_feedback, viewGroup, false);
        return new FeedbackAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FeedbackAdapter.ViewHolder viewHolder, int i) {

        final Feedback model = feedbacks.get(i);

        viewHolder.tvName.setText(model.getName());
        viewHolder.tvEmailFd.setText(model.getEmail());


        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
                builderSingle.show();
                return true;
            }
        });

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, FeedbackdetailActivity.class);
                intent.putExtra("Name", model.getName());
                intent.putExtra("Email", model.getEmail());
                intent.putExtra("Messagefb", model.getMessagefb());
                intent.putExtra("id", model.getId());

                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return feedbacks.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvName, tvEmailFd;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvEmailFd = itemView.findViewById(R.id.tvEmailFd);


        }

    }

}
