package com.example.kangaroo.adgreencars.model;

public class Feedback {
    private String Name;
    private String Messagefb;
    private String Email;

    public float getRating() {
        return Rating;
    }

    public void setRating(float rating) {
        Rating = rating;
    }

    private  float Rating;

    private String id;

    public Feedback() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getMessagefb() {
        return Messagefb;
    }

    public void setMessagefb(String Messagefb) {
        this.Messagefb = Messagefb;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) { this.Email = Email; }


    public String getId() {
        return id;
    }

    public void setId(String id) { this.id = id; }


}
