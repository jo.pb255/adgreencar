package com.example.kangaroo.adgreencars.model;

public class Point {
    private String statusplayment;
    private String DateandTime;
    private String id;
    public Point() {
    }

    public String getStatusplayment() {
        return statusplayment;
    }

    public void setStatusplayment(String statusplayment) {
        this.statusplayment = statusplayment;
    }

    public String getDateandTime() { return DateandTime; }

    public void setDateandTime(String DateandTime) {
        this.DateandTime = DateandTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) { this.id = id; }


}
