package com.example.kangaroo.adgreencars;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class StudentdetailActivity  extends AppCompatActivity {

    private TextView tvShowName, tvShow_StudentID, tvShowBranch, tvShowFaculty, tvShowEmail, tvShowPassword;
    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studentdetail);

        toolbar = findViewById(R.id.toolbar_list_phone_activity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Data User");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvShowName = findViewById(R.id.tvShowName);
        tvShow_StudentID = findViewById(R.id.tvShow_StudentID);
        tvShowBranch = findViewById(R.id.tvShowBranch);
        tvShowFaculty = findViewById(R.id.tvShowFaculty);
        tvShowEmail = findViewById(R.id.tvShowEmail);
        tvShowPassword = findViewById(R.id.tvShowPassword);


        String Name = getIntent().getStringExtra("Name");
        String StudentID = getIntent().getStringExtra("StudentID");
        String Branch = getIntent().getStringExtra("Branch");
        String Faculty = getIntent().getStringExtra("Faculty");
        String Email = getIntent().getStringExtra("Email");
        String Password = getIntent().getStringExtra("Password");
        String id = getIntent().getStringExtra("id");


        tvShowName.setText(Name);
        tvShow_StudentID.setText(StudentID);
        tvShowBranch.setText(Branch);
        tvShowFaculty.setText(Faculty);
        tvShowEmail.setText(Email);
        tvShowPassword.setText(Password);

        Log.d("asdasdas", "onCreate: "+Name);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        startActivity(new Intent(getApplicationContext(), ListStudentActivity.class));
        return true;
    }
}
