package com.example.kangaroo.adgreencars;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class EditStudentDataActivity extends AppCompatActivity {

    private EditText edtName, edtStudentID, edtBranch, edtFaculty, edtEmail, edtPassword;
    private Button btnInsertPhone, btnCancle;


    private FirebaseUser firebaseUser;
    private DatabaseReference reference;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_student_data);

        toolbar = findViewById(R.id.toolbar_edit_phone_data);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Update User");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        edtName = findViewById(R.id.edtName);
        edtStudentID = findViewById(R.id.edtStudentID);
        edtBranch = findViewById(R.id.edtBranch);
        edtFaculty = findViewById(R.id.edtFaculty);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);

        btnInsertPhone = findViewById(R.id.btnInsertPhone);
        btnCancle = findViewById(R.id.btnCancle);

        String Name = getIntent().getStringExtra("Name");
        String StudentID = getIntent().getStringExtra("StudentID");
        String Branch = getIntent().getStringExtra("Branch");
        String Faculty = getIntent().getStringExtra("Faculty");
        String Email = getIntent().getStringExtra("Email");
        String Password = getIntent().getStringExtra("Password");


        edtName.setText(Name);
        edtStudentID.setText(StudentID);
        edtBranch.setText(Branch);
        edtFaculty.setText(Faculty);
        edtEmail.setText(Email);
        edtPassword.setText(Password);


        btnInsertPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Name = edtName.getText().toString();
                String StudentID = edtStudentID.getText().toString();
                String Branch = edtBranch.getText().toString();
                String Faculty = edtFaculty.getText().toString();
                String Email = edtEmail.getText().toString();
                String Password = edtPassword.getText().toString();
                String id = getIntent().getStringExtra("id");

                updateData(Name, StudentID, Branch, Faculty, Email, Password, id);
            }
        });

        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    private void updateData(final String Name, final String StudentID, final String Branch, final String Faculty, final String Email,
                            final String Password, final String id) {
        reference = FirebaseDatabase.getInstance().getReference("User").child(id);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                //แก้ไขข้อมูล
               dataSnapshot.getRef().child("/Name/").setValue(Name);
               dataSnapshot.getRef().child("/StudentID/").setValue(StudentID);
                dataSnapshot.getRef().child("/Branch/").setValue(Branch);
                dataSnapshot.getRef().child("/Faculty/").setValue(Faculty);
               dataSnapshot.getRef().child("/Email/").setValue(Email);
               dataSnapshot.getRef().child("/Password/").setValue(Password);
               EditStudentDataActivity.this.finish();
               Toast.makeText(EditStudentDataActivity.this, "Update Successful", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        startActivity(new Intent(getApplicationContext(), ListStudentActivity.class));
        return true;
    }


}
