package com.example.kangaroo.adgreencars;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kangaroo.adgreencars.model.Point;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class PointAdapter extends RecyclerView.Adapter<PointAdapter.ViewHolder> {

    Context context;
    ArrayList<Point> points;

    public PointAdapter(Context context, ArrayList<Point> points) {
        this.context = context;
        this.points = points;
    }


    @NonNull
    @Override
    public PointAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.activity_point, viewGroup, false);
        return new PointAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PointAdapter.ViewHolder viewHolder, int i) {

        final Point model = points.get(i);

        viewHolder.tvPoint.setText(model.getStatusplayment());
        viewHolder.tvStudentID.setText(model.getDateandTime());



        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
                String[] animals = {"Delete"};
                builderSingle.setItems(animals, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 1: // แก้ไข
//                                Intent intent = new Intent(context, EditStudentDataActivity.class);
//                                intent.putExtra("Name", model.getName());
//                                intent.putExtra("StudentID", model.getStudentID());
//                                intent.putExtra("Branch", model.getBranch());
//                                intent.putExtra("Faculty", model.getFaculty());
//                                intent.putExtra("Email", model.getEmail());
//                                intent.putExtra("Password", model.getPassword());
//                                intent.putExtra("id", model.getId());
//                                context.startActivity(intent);
//                                break;
//
                            case 0: // ลบ

                                FirebaseDatabase.getInstance().getReference("Point").child(model.getId())
                                        .removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Toast.makeText(context, "Delete Successful", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, "Delete unsuccessful", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                                break;
                        }
                    }
                });




                builderSingle.show();
                return true;
            }
        });

//        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(context, FeedbackdetailActivity.class);
//                intent.putExtra("statusplayment", model.getStatusplayment());
////                intent.putExtra("Email", model.getEmail());
////                intent.putExtra("Messagefb", model.getMessagefb());
//                intent.putExtra("id", model.getId());
//
//                context.startActivity(intent);
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return points.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvPoint, tvStudentID;

        public ViewHolder(View itemView) {
            super(itemView);
            tvPoint = itemView.findViewById(R.id.tvPoint);
            tvStudentID = itemView.findViewById(R.id.tvStudentID);


        }

    }

}
