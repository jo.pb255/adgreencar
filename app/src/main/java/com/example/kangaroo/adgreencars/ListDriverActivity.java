package com.example.kangaroo.adgreencars;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kangaroo.adgreencars.model.DriverModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ListDriverActivity extends AppCompatActivity {

    private RecyclerView rvListDriver;
    private Toolbar toolbar;
    private FloatingActionButton btn_Floating;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference reference;

    private ArrayList<DriverModel> models;
    private DriverAdapter driverAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_data_driver);

        toolbar = findViewById(R.id.toolbar_list_phone_activity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Drivers");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        btn_Floating =findViewById(R.id.btn_Floating);
        rvListDriver = findViewById(R.id.rvListDriver);
        rvListDriver.setHasFixedSize(true);
        rvListDriver.setNestedScrollingEnabled(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvListDriver.setLayoutManager(linearLayoutManager);

        btn_Floating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),InsertDriverActivity.class));
            }
        });

        mAuth = FirebaseAuth.getInstance();
        readDriverDetail();


    }

          private void readDriverDetail(){

            models = new ArrayList<>();
            reference = FirebaseDatabase.getInstance().getReference("Drivers");
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    models.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        DriverModel phoneModel = snapshot.getValue(DriverModel.class);
                        //Log.d("asdasd", "onDataChange: "+phoneModel.getBrand());

                        models.add(phoneModel);
                    }

                    driverAdapter = new DriverAdapter(ListDriverActivity.this, models);
                    rvListDriver.setAdapter(driverAdapter);
                    driverAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        return true;
    }
}
