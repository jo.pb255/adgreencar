package com.example.kangaroo.adgreencars;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.kangaroo.adgreencars.model.Feedback;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class FeedbackdetailActivity extends AppCompatActivity {

    private TextView tvShowName, tvShowMessage, tvShowEmail;
    private Toolbar toolbar;
    private RatingBar ratingBar;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private Query reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedbackdetail);

        toolbar = findViewById(R.id.toolbar_phonedetail);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Feedback");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        tvShowName = findViewById(R.id.tvShowName);
        tvShowEmail = findViewById(R.id.tvShowEmail);
        tvShowMessage = findViewById(R.id.tvShowMessage);
        ratingBar= findViewById(R.id.ratingBar);

        String Name = getIntent().getStringExtra("Name");
        String Messagefb = getIntent().getStringExtra("Messagefb");
        String Email = getIntent().getStringExtra("Email");
        String id = getIntent().getStringExtra("id");


        tvShowName.setText(Name);
        tvShowEmail.setText(Email);
        tvShowMessage.setText(Messagefb);

        Log.d("asdasdas", "onCreate: "+Name);
        mAuth = FirebaseAuth.getInstance();
        firebaseUser = mAuth.getCurrentUser();

        reference = FirebaseDatabase.getInstance().getReference("Feedback").orderByChild("id").equalTo(id);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    final Feedback feedback = snapshot.getValue(Feedback.class);


                    float rating = feedback.getRating();
                    ratingBar.setRating(rating);
                    Log.d("45878787sm", rating+"");



                }


            }



            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });





    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        startActivity(new Intent(getApplicationContext(), ListFeedbackActivity.class));
        return true;


    }
}
