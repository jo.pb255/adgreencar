package com.example.kangaroo.adgreencars;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kangaroo.adgreencars.model.Student;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.ViewHolder> {

    Context context;
    ArrayList<Student> students;

    public StudentAdapter(Context context, ArrayList<Student> students) {
        this.context = context;
        this.students = students;
    }


    @NonNull
    @Override
    public StudentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.activity_student, viewGroup, false);
        return new StudentAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final StudentAdapter.ViewHolder viewHolder, int i) {

        final Student model = students.get(i);

        viewHolder.tvName.setText(model.getName());
        viewHolder.tvStudentID.setText(model.getStudentID());


        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
                String[] animals = {"Edit", "Delete"};
                builderSingle.setItems(animals, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0: // แก้ไข
                                Intent intent = new Intent(context, EditStudentDataActivity.class);
                                intent.putExtra("Name", model.getName());
                                intent.putExtra("StudentID", model.getStudentID());
                                intent.putExtra("Branch", model.getBranch());
                                intent.putExtra("Faculty", model.getFaculty());
                                intent.putExtra("Email", model.getEmail());
                                intent.putExtra("Password", model.getPassword());
                                intent.putExtra("id", model.getId());
                                context.startActivity(intent);
                                break;
                            case 1: // ลบ

                                FirebaseDatabase.getInstance().getReference("User").child(model.getId())
                                        .removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Toast.makeText(context, "Delete Successful", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, "Delete unsuccessful", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                                break;
                        }
                    }
                });
                builderSingle.show();
                return true;
            }
        });

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, StudentdetailActivity.class);
                intent.putExtra("Name", model.getName());
                intent.putExtra("StudentID", model.getStudentID());
                intent.putExtra("Branch", model.getBranch());
                intent.putExtra("Faculty", model.getFaculty());
                intent.putExtra("Email", model.getEmail());
                intent.putExtra("Password", model.getPassword());
                intent.putExtra("id", model.getId());

                context.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvName, tvStudentID;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvStudentID = itemView.findViewById(R.id.tvStudentID);


        }

    }

}
