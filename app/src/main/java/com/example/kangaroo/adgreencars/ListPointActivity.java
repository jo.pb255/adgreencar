package com.example.kangaroo.adgreencars;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kangaroo.adgreencars.model.Point;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ListPointActivity extends AppCompatActivity {
    private RecyclerView rvListStudent;
    private Toolbar toolbar;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference reference;

    private ArrayList<Point> models;
    private PointAdapter pointAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_point);

        toolbar = findViewById(R.id.toolbar_list_phone_activity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Point");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        rvListStudent = findViewById(R.id.rvListStudent);
        rvListStudent.setHasFixedSize(true);
        rvListStudent.setNestedScrollingEnabled(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvListStudent.setLayoutManager(linearLayoutManager);

        mAuth = FirebaseAuth.getInstance();

        readStudentDetail();

    }
    private void readStudentDetail() {

        models = new ArrayList<>();
        reference = FirebaseDatabase.getInstance().getReference("Point");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                models.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Point point = snapshot.getValue(Point.class);
                    //Log.d("asdasd", "onDataChange: "+phoneModel.getBrand());

                    models.add(point);
                }

                pointAdapter = new PointAdapter(ListPointActivity.this,models);
                rvListStudent.setAdapter(pointAdapter);
                pointAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        return true;
    }

    }
