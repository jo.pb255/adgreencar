package com.example.kangaroo.adgreencars;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kangaroo.adgreencars.model.Feedback;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ListFeedbackAdminActivity extends AppCompatActivity {
    private RecyclerView rvListFeedback;
    private Toolbar toolbar;

    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference reference;

    private ArrayList<Feedback> models;
    private FeedbackAdminAdapter feedbackAdminAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_feedbackadmin);

        toolbar = findViewById(R.id.toolbar_list_phone_activity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Feedback");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        rvListFeedback = findViewById(R.id.rvListFeedback);
        rvListFeedback.setHasFixedSize(true);
        rvListFeedback.setNestedScrollingEnabled(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvListFeedback.setLayoutManager(linearLayoutManager);

        mAuth = FirebaseAuth.getInstance();

        readFeedbackDetail();

    }
    private void readFeedbackDetail() {

        models = new ArrayList<>();
        reference = FirebaseDatabase.getInstance().getReference("Feedback");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                models.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Feedback feedback = snapshot.getValue(Feedback.class);
                    //Log.d("asdasd", "onDataChange: "+phoneModel.getBrand());

                    models.add(feedback);
                }

                feedbackAdminAdapter = new FeedbackAdminAdapter(ListFeedbackAdminActivity.this,models);
                rvListFeedback.setAdapter(feedbackAdminAdapter);
                feedbackAdminAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        return true;
    }

    }
