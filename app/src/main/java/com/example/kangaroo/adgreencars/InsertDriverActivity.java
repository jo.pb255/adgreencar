package com.example.kangaroo.adgreencars;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class InsertDriverActivity extends AppCompatActivity {
    private EditText edtFullname, edtPhone, edtAddress, edtEmail, edtRegisterPassword;
    private Button btnRegisterSubmit;
    private ProgressDialog mLogProgress;
    private DatabaseReference reference;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private Toolbar toolbar;

    String userID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_driver);

        toolbar = findViewById(R.id.toolbar_list_phone_activity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(" Add Drivers");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        edtFullname = findViewById(R.id.edtFullname);
        edtPhone = findViewById(R.id.edtPhone);
        edtEmail = findViewById(R.id.edtEmail);
        edtAddress = findViewById(R.id.edtAddress);
        edtRegisterPassword = findViewById(R.id.edtRegisterPassword);
        btnRegisterSubmit = findViewById(R.id.btnRegisterSubmit);

        mAuth = FirebaseAuth.getInstance();
        mLogProgress = new ProgressDialog(this);
        btnRegisterSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //ดึงค่าจาก Edittext
                String Name = edtFullname.getText().toString();
                String Phone = edtPhone.getText().toString();
                String Address = edtAddress.getText().toString();
                String Email = edtEmail.getText().toString();
                String Password = edtRegisterPassword.getText().toString();

                if (!TextUtils.isEmpty(Name) || !TextUtils.isEmpty(Phone) || !TextUtils.isEmpty(Address) || !TextUtils.isEmpty(Email)
                        || !TextUtils.isEmpty(Password)) {
//                    mLogProgress.setTitle("Registering User");
                    mLogProgress.setMessage("loading...");
                    mLogProgress.setCanceledOnTouchOutside(false);
                    mLogProgress.show();

                    register(Name, Phone, Address, Email, Password);
                    Log.d("ssa", Name);
                }

            }
        });




    }
    private void register(final String Name, final String Phone, final String Address, final String Email, final String Password
    ) {

        //สร้าง Email และ Password
        mAuth.createUserWithEmailAndPassword(Email, Password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            //ปิด Progress
                            mLogProgress.dismiss();

                            //เชื่อมต่อไปยังข้อมูล User
                            firebaseUser = mAuth.getCurrentUser();
                            assert firebaseUser != null;

                            //ดึง key มาเก็บไว้ในตัวแปล
                            String userID = firebaseUser.getUid();

                            //สร้างตาราง Admin ขึ้นมาเพื่อเก็บข้อมูล
                            reference = FirebaseDatabase.getInstance().getReference("Drivers").child(userID);
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("id", userID);
                            hashMap.put("Name", Name);
                            hashMap.put("Phone", Phone);
                            hashMap.put("Address", Address);
                            hashMap.put("Email", Email);
                            hashMap.put("Password", Password);
                            hashMap.put("Adtype", "Drivers");
                            reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(InsertDriverActivity.this, "Register Successful", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(InsertDriverActivity.this, ListDriverActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            });

                        }


                    }
                });
    }
    private void InsertData(String Name,String Phone, String Address, String Email, String Password) {

        //สรา้ง id ให้ item
        reference = FirebaseDatabase.getInstance().getReference("Drivers");
        Map<String, Object> hashMap2 = new HashMap<>();
        userID = reference.push().getKey();
        reference.updateChildren(hashMap2);
        reference = reference.child(userID);
        Map<String, Object> add = new HashMap<>();
        add.put("id", userID);
        add.put("Name", Name);
        add.put("Phone", Phone);
        add.put("Address", Address);
        add.put("Email", Email);
        add.put("Password", Password);
        reference.updateChildren(add);
        finish();
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        return true;
    }
}
