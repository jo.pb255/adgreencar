package com.example.kangaroo.adgreencars.model;

public class DriverModel {
    private String Name;
    private String Phone;
    private String Address;
    private String Email;
    private String Password;
    private String id;
    private String Adtype;
    public DriverModel() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }
    public String getAddress() {
        return Address;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) { this.Email = Email; }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) { this.Password = Password; }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdtype() { return Adtype; }

    public void setAdtype(String Adtype) { this.Adtype = Adtype; }


}
