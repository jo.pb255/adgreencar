package com.example.kangaroo.adgreencars;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class EditDriverDataActivity extends AppCompatActivity {

    private EditText edtName, edtPhone, edtAddress, edtEmail, edtPassword;
    private Button btnInsertPhone, btnCancle;


    private FirebaseUser firebaseUser;
    private DatabaseReference reference;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_driver_data);
        toolbar = findViewById(R.id.toolbar_edit_phone_data);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Update Driver");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        edtName = findViewById(R.id.edtName);
        edtPhone = findViewById(R.id.edtPhone);
        edtAddress = findViewById(R.id.edtAddress);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);

        btnInsertPhone = findViewById(R.id.btnInsertPhone);
        btnCancle = findViewById(R.id.btnCancle);

        String Name = getIntent().getStringExtra("Name");
        String Phone = getIntent().getStringExtra("Phone");
        String Address = getIntent().getStringExtra("Address");
        String Email = getIntent().getStringExtra("Email");
        String Password = getIntent().getStringExtra("Password");


        edtName.setText(Name);
        edtPhone.setText(Phone);
        edtAddress.setText(Address);
        edtEmail.setText(Email);
        edtPassword.setText(Password);


        btnInsertPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Name = edtName.getText().toString();
                String Phone = edtPhone.getText().toString();
                String Address = edtAddress.getText().toString();
                String Email = edtEmail.getText().toString();
                String Password = edtPassword.getText().toString();
                String id = getIntent().getStringExtra("id");

                updateData(Name, Phone, Address, Email, Password, id);
            }
        });

        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    private void updateData(final String Name, final String Phone, final String Address, final String Email,
                            final String Password, final String id) {
        reference = FirebaseDatabase.getInstance().getReference("Drivers").child(id);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                //แก้ไขข้อมูล
               dataSnapshot.getRef().child("/Name/").setValue(Name);
               dataSnapshot.getRef().child("/Phone/").setValue(Phone);
                dataSnapshot.getRef().child("/Address/").setValue(Address);
               dataSnapshot.getRef().child("/Email/").setValue(Email);
               dataSnapshot.getRef().child("/Password/").setValue(Password);
               EditDriverDataActivity.this.finish();
               Toast.makeText(EditDriverDataActivity.this, "Update Successful", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        startActivity(new Intent(getApplicationContext(), ListDriverActivity.class));
        return true;
    }


}
