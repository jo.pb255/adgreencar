package com.example.kangaroo.adgreencars;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.kangaroo.adgreencars.model.DriverModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class LoginActivity extends AppCompatActivity {
    private EditText edtEmail, edtPassword;
    private Button btnLogin;
    private TextView tvregister;
    private ProgressDialog mLogProgress;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        tvregister = findViewById(R.id.tvregister);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        btnLogin = findViewById(R.id.btnLogin);

        mLogProgress = new ProgressDialog(this);

        mAuth = FirebaseAuth.getInstance();
//        if (mAuth.getCurrentUser() != null) {
//            finish();
//            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
//        }

        //เช็คค่า User

//        tvregister.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(getApplicationContext(),RegisterUserActivity.class));
//            }
//        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Email = edtEmail.getText().toString();
                String Password = edtPassword.getText().toString();

                if (!TextUtils.isEmpty(Email) || !TextUtils.isEmpty(Password)) {
                    mLogProgress.setTitle("loading...");
                    mLogProgress.setMessage("Please wait while we check your credentials.");
                    mLogProgress.setCanceledOnTouchOutside(false);
                    mLogProgress.show();

                    loginUser(Email, Password);
                }
            }
        });



    }
    private void loginUser(String username, String password) {

        mAuth.signInWithEmailAndPassword(username, password)
                .addOnCompleteListener(task -> {

                    if (task.isSuccessful()) {
                        mLogProgress.dismiss();
                        finish();

                        firebaseUser = mAuth.getCurrentUser();
//                             assert firebaseUser != null;

                        //ดึง key มาเก็บไว้ในตัวแปล
                        String userID = firebaseUser.getUid();
                        //reference สองอันข้างล่างคือ เวลาเราจะ path เข้าไปหาข้อมูลของ user ต้องทำแบบนี้ทุกครั้ง
                        reference = FirebaseDatabase.getInstance().getReference("Drivers").child(userID);
                        reference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    DriverModel driverModel = dataSnapshot.getValue(DriverModel.class);
                                    Log.d("onResume", "onDataChange: "+driverModel.getAdtype());
                                    if(driverModel.getAdtype().equals("Drivers")){
                                        Toast.makeText(LoginActivity.this, "Adtype :"+driverModel.getAdtype(), Toast.LENGTH_SHORT).show();
                                        //  intent ไปหน้าแอดมินสร้างขึ้นมาใหม่เอง

                                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                        finish();

                                    }

                                    mLogProgress.dismiss();
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    } else {
                        mLogProgress.hide();
                         Toast.makeText(LoginActivity.this, "Cannot Sign in. ", Toast.LENGTH_SHORT).show();


                    }
                });
    }
}

